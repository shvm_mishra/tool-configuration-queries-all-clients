
--- Mapping leads in outreach in to salesforce and checking KPIs at sequence ID level

with task_data as (
  select t.id mail_id, t.created_at , t.subject , t.relationship_sequence_id seq_id, s."name" , s.description ,
  t.relationship_opportunity_id , t.relationship_prospect_id , dc.id lead_id, t.relationship_sequence_step_id
  from claroty_outreach.mailing t --claroty_outreach.task t
  left join claroty_outreach."sequence" s on t.relationship_sequence_id = s.id
  left join claroty_outreach.data_connection dc on dc.parent_id = t.relationship_prospect_id
  where relationship_sequence_id is not null and dc."type" = 'Lead'			--- 1645 distinct leads with emails | 4871 emails
 --  and dc.id = '00Q4H00000kFHf3UAG'
)
, tat_data as (
  select l.id lead_id ,
  	l.created_date ,mql_time, b.sql_time,
  	extract(epoch from sql_time) - extract(epoch from l.created_date) sql_tat,
  	extract(epoch from mql_time) - extract(epoch from l.created_date) mql_tat
  from claroty_salesforce.lead l
  left join
  (
  	select lead_id , min(lh.created_date) mql_time
  	from claroty_salesforce.lead_history lh
  	where new_value = 'MQL'
  	group by 1			--- 7182 leads with mql timestamp
  ) a on l.id = a.lead_id
  left join
  (
  	select lead_id , min(lh.created_date) sql_time
  	from claroty_salesforce.lead_history lh
  	where new_value = 'SQL'
  	group by 1			--- 4338 leads with sql timestamp
  ) b on l.id = b.lead_id
  -- where sql_time is not null
)
, final_data as (
	select -- distinct lead_id
		seq_id, seq_name, lead_id,
		avg(a.sql_tat/60) sql_time,
		avg(a.mql_tat/60) mql_time
	from (
		select mail_id, b.created_at mail_created_time, seq_id,
		  b.name seq_name, a.lead_id, a.created_date, a.sql_tat, a.mql_tat
		from tat_data a
		left join task_data b on a.lead_id = b.lead_id
	) a
	where seq_id is not null
		-- and sql_tat is not null
		-- and mql_tat is not null
	group by 1,2,3
)
-- select count(distinct lead_id) cn from task_data where lead_id in (select lead_id from tat_data where mql_time is not null )  -- 00Q4H00000nSFs5UAG lead id present in outreach but not in salesforce
, seq_count_data as (
  select lead_id, count(distinct seq_id) seq_count
  from final_data group by 1 order by 2 desc
)
select seq_id, seq_name,
	count(distinct lead_id)::numeric all_leads,
	count(distinct(case when mql_time is not null then lead_id else null end))::numeric mql_leads,
	round(count(distinct(case when mql_time is not null then lead_id else null end))::numeric / count(distinct lead_id)::numeric,2) mql_cr,
	round(avg(case when mql_time is not null then mql_time else 0 end)::numeric,2) avg_mql_tat,
	count(distinct(case when sql_time is not null then lead_id else null end))::numeric sql_leads,
	round(count(distinct(case when sql_time is not null then lead_id else null end))::numeric / count(distinct lead_id)::numeric,2) sql_cr,
	round(avg(case when mql_time is not null then mql_time else 0 end)::numeric,2) avg_sql_tat
from final_data
where lead_id not in (select lead_id from seq_count_data where seq_count > 1)
group by 1,2
