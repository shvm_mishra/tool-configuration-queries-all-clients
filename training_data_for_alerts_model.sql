


with completed_opps as (
	select id 
		,"type" 
		,lead_source 
		,date(created_date) as first_creation_date
		,case when stage_name in ('Closed Won') then 1 else 0 end as is_won
		,o.last_stage_change_date actual_close_date
	from claroty_salesforce.opportunity o 
	where stage_name  in ('Closed No Decision','Closed Won', 'Closed Lost')
	and is_deleted = false
)
, opp_his1 as (
	select * 
		,case when prev_rn_stage is not null and  cast(rn-1 as int) != cast(prev_rn_stage as int)  then 1 else 0 end as is_rework
	from
	(
		select *
			,lag(rn) over (partition by opportunity_id,stage_name order by created_date) as prev_rn_stage
			,coalesce(extract(epoch from next_stage_date-created_date )/3600, extract(epoch from current_timestamp-created_date)/3600) time_in_stage
			,row_number() over(partition by opportunity_id, stage_name order by created_date desc ) as stg_rn
		from
		(
			select oh.* 
				,lead(created_date) over(partition by opportunity_id order by created_date) as next_stage_date
				,row_number() over(partition by opportunity_id order by created_date) as rn
				,co.type
				,co.lead_source
				,co.first_creation_date
				,co.is_won
				,co.actual_close_date
			from claroty_salesforce.opportunity_history oh 
			inner join completed_opps co 
			on oh.opportunity_id  = co.id
		) a
	) b
	order by opportunity_id, created_date
)
, opp_his_aggr as (
	select opportunity_id
		,stage_name
		,sum(time_in_stage) total_time_in_stage
		,avg(time_in_stage) avg_time_in_stage
		,count(distinct created_date) n_times_in_stage
		,count(distinct amount) amount_change_count
		,count(distinct expected_revenue) expected_revenue_change_count
		,count(distinct close_date) close_date_change_count
		,count(distinct forecast_category) forecast_category_change_count
		,count(distinct currency_iso_code) currency_iso_code_change_count
		,max(is_rework) is_rework
		,sum(is_rework) total_rework
		,min(created_date) first_stage_creation_date
	from opp_his1
	group by 1,2
)
,  mail_tp_outreach as (
	select
		dc.id as opp_id, count(*) email_touchpoints
	from
		claroty_outreach.mailing m
	join claroty_outreach.data_connection dc on dc.parent_id = m.relationship_opportunity_id 
	left join claroty_salesforce.opportunity l on dc.id = l.id
	where
		1 = 1
		and m.relationship_prospect_id is not null
		and dc.type = 'Opportunity'
	group by 1
)
,  call_tp_outreach as (
	select
		dc.id as opp_id, count(*) call_touchpoints
	from
		claroty_outreach.call m
	join claroty_outreach.data_connection dc on dc.parent_id = m.relationship_opportunity_id 
	left join claroty_salesforce.opportunity l on dc.id = l.id
	where
		1 = 1
		and m.relationship_prospect_id is not null
		and dc.type = 'Opportunity'
	group by 1
)
, salesforce_events as (
	with events as (
		select
			e."type" ,
			e.what_id as event_lead ,
			e.what_id as opportunity_id ,
			e.activity_date_time,
			e.who_id,
			o.contact_id,
			o.is_won::text , is_closed::text,
			upper(c.title) title 
		from
			claroty_salesforce."event" e
			left join claroty_salesforce.opportunity o on e.what_id = o.id
			left join claroty_salesforce.contact c on e.who_id = c.id 
		where
			what_id like '%006%'
			and who_id ilike '%003%'
	)
	, top_titles as (
		select distinct upper(title) title
		from events
		where upper(title) in ('ACTING CISO & DIRECTOR, INFORMATION ASSU','ASSISTANT GENERAL MANAGER','GENERAL MANAGER')
			or title ilike ('%director%')
			or title ilike ('%ceo%')
			or title ilike ('%officer%')
			or title ilike ('%ciso%')
			or title ilike ('%chief%')
			or title ilike ('%cio%')
			or title ilike ('%founder%')
			or (length(title) = 3 and title ilike 'C%')
			or title ilike ('%VP%') 
	)	
	select opportunity_id, 
		sum(case when upper(e.title) in (select title from top_titles) then 1 else 0 end) as top_level_contact_events,
		sum(case when upper(e.title) not in (select title from top_titles) then 1 else 0 end) as low_level_contact_events
		, count(*) total_events
	from events e
	group by 1 order by 1
)
, final_output as (
	select id
		,oh.opportunity_id
		,date(first_stage_creation_date) first_stage_creation_date
		,date(created_date) as last_stage_creation_date
		,date(actual_close_date) actual_close_date
		,date(actual_close_date) - date(first_stage_creation_date) as days_to_close
		,oh.stage_name
		,lag(oh.stage_name) over (partition by oh.opportunity_id order by oh.created_date) as prev_stage_name
		,amount
		,expected_revenue
		,close_date
		,forecast_category
		,probability 
		,currency_iso_code
		,total_time_in_stage
		,avg_time_in_stage
		,n_times_in_stage
		,amount_change_count
		,expected_revenue_change_count
		,close_date_change_count
		,forecast_category_change_count
		,currency_iso_code_change_count
		,oha.is_rework
		,oha.total_rework
		,oh.type
		,oh.lead_source
		,oh.first_creation_date opp_creation_date
		,date(actual_close_date) - oh.first_creation_date as days_since_opp_creation
		,oh.is_won
		,coalesce (email_touchpoints,0) email_touchpoints
		,coalesce (call_touchpoints,0) call_touchpoints
		,coalesce (top_level_contact_events,0) top_level_contact_events
		,coalesce (low_level_contact_events,0) low_level_contact_events
		,coalesce (total_events,0) total_events
	from opp_his1 oh
	left join opp_his_aggr oha on oh.opportunity_id = oha.opportunity_id and oh.stage_name = oha.stage_name
	left join mail_tp_outreach mtp on oh.opportunity_id = mtp.opp_id
	left join call_tp_outreach ctp on oh.opportunity_id = ctp.opp_id
	left join salesforce_events sev on oh.opportunity_id = sev.opportunity_id
	where oh.stg_rn = 1 and oh.stage_name not in ('Closed No Decision','Closed Won', 'Closed Lost')
	order by opportunity_id, created_date
)
select * from final_output
