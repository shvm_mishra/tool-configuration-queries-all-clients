

--- 1. SDR Lead Volumes - Product Query
select $group_by_head,
    count(distinct (case when new_value = 'MQL' then id else null end)) as "Raw MQLs",
	count(distinct (case when status not in ('Convert to Contact','Not Relevant','Partner Qualified') and new_value = 'MQL' then id else null end)) as "Workable MQLs",
	count(distinct (case when new_value = 'SDR Qualified' then id else null end)) as "SDR Leads",
	count(distinct (case when new_value = 'Sales Accepted' then id else null end)) "SAL Leads"
from 
(
	select a.type, l.region_c ,l.lead_source , l.pi_campaign_c , l.id , l.created_date lead_created , 
		l.lead_type_c , l.type_of_business_c , lh.created_date history_created, extract (year from lh.created_date) created_year, lh.field , lh.new_value , l.status ,
		rank() over (partition by l.id, new_value order by lh.created_date) rnk 
	from claroty_salesforce."lead" l
	left join claroty_salesforce.lead_history lh on l.id = lh.lead_id 
	left join claroty_salesforce.account a on l.converted_account_id = a.id 
	-- left join claroty_salesforce.campaign c on c.id = l.
	where field = 'Status' -- and extract (year from lh.created_date) in (2021,2022)
		and l.is_deleted = false
	order by l.id, lh.created_date 
) a 
where rnk = 1 and extract (year from history_created) in (2021,2022)
and $where_clause_filters and $time_window 
group by $group_by_tail


--- 2. SDR Lead Conversion Rates - Product Query
select $group_by_head,
	round(count(distinct (case when status not in ('Convert to Contact','Not Relevant','Partner Qualified') and new_value = 'MQL' then id else null end))::numeric / count(distinct (case when new_value = 'MQL' then id else null end))::numeric,2) as "Raw to Workable MQL CR",
	round(count(distinct (case when new_value = 'SDR Qualified' then id else null end))::numeric/count(distinct (case when new_value = 'MQL' then id else null end))::numeric,2) as "Raw to SDR CR",
	round(count(distinct (case when new_value = 'Sales Accepted' then id else null end))::numeric/round(count(distinct (case when new_value = 'SDR Qualified' then id else null end))::numeric),2) as "SDR to SAL CR"
from 
(
	select a.type, l.region_c ,l.lead_source , l.pi_campaign_c , l.id , l.created_date lead_created , 
		l.lead_type_c , l.type_of_business_c , lh.created_date history_created ,
        extract (year from lh.created_date) created_year, lh.field , lh.new_value , l.status ,
		rank() over (partition by l.id, new_value order by lh.created_date) rnk 
	from claroty_salesforce."lead" l
	left join claroty_salesforce.lead_history lh on l.id = lh.lead_id 
	left join claroty_salesforce.account a on l.converted_account_id = a.id 
	-- left join claroty_salesforce.campaign c on c.id = l.
	where field = 'Status' -- and extract (year from lh.created_date) in (2021,2022)
		and l.is_deleted = false
	order by l.id, lh.created_date 
) a 
where rnk = 1 and extract (year from history_created) in (2021,2022)
and $where_clause_filters and $time_window 
group by $group_by_tail


-- 3. Opp Central - Opportunity Custom KPIs - Product Query
select $group_by_head,
	count(opp_id) as "Opps Created", 
	concat(round((sum(amount*cr)::decimal)/1000000,1),'M') as "Opp Created Amount($)",
	concat(round((sum(case when stage_name = 'Closed Won' then (amount*cr)::decimal else 0 end))/1000000,1),'M') as "Closed Won Amount($)"
from 
(
	select a.*, coalesce(1 / d.conversion_rate, 1)as cr
	from 
	(
	  select o.id opp_id, o.stage_name , o.amount , extract (year from o.created_date) as "Year Created",
	  	o.close_date , o.is_won , o.created_date , o.currency_iso_code,
	  	case when stage_name = 'Closed Won' then (case when extract(hour from last_stage_change_date) < 10 
	  												then (last_stage_change_date::timestamp + '-1day') 
	  												else (last_stage_change_date)end)
	  		else current_timestamp end as last_date
	  from claroty_salesforce.opportunity o 
	  -- left join claroty_salesforce.opportunity_field_history oh on o.id = oh.opportunity_id 
	  where extract (year from o.created_date) in (2021,2022)
	  	and o.is_deleted = false 
	) a
	left join claroty_salesforce.dated_conversion_rate d on
		a.currency_iso_code = d.iso_code
		and a.last_date::date = d.created_date::date 
) final 
where $where_clause_filters and $time_window 
group by $group_by_tail


--- 4 Rep performance Metrics - Product Query
with mail_tp as (
	select user_id_salesforce, usr_name, count(distinct mail_pk) mail_count
	from (
		select m.id mail_pk, m.created_at, m.subject , m.relationship_prospect_id , p.id , usr.id , usr."name" usr_name, dc.id user_id_salesforce
		from claroty_outreach.mailing m 
		left join claroty_outreach.prospect p on m.relationship_prospect_id = p.id 
		left join claroty_outreach.users usr on p.relationship_owner_id = usr.id 
		left join claroty_outreach.data_connection dc on dc.parent_id = usr.id 
		where usr.id is not null and parent_type = 'user'
			and extract (year from m.created_at) in (2022)
	) a 
	group by 1,2
)
, call_tp as (
	select user_id_salesforce, usr_name, count(distinct mail_pk) call_count
	from (
		select m.id mail_pk, m.created_at, m.relationship_prospect_id , p.id , usr.id , usr."name" usr_name, dc.id user_id_salesforce
		from claroty_outreach."call" m 
		left join claroty_outreach.prospect p on m.relationship_prospect_id = p.id 
		left join claroty_outreach.users usr on p.relationship_owner_id = usr.id 
		left join claroty_outreach.data_connection dc on dc.parent_id = usr.id 
		where usr.id is not null and parent_type = 'user'
			and extract (year from m.created_at) in (2022)
	) a 
	group by 1,2
)
, sl_kpis as (
	select user_id, rep,
		count(distinct converted_opportunity_id) sqo,
		count(distinct (case when new_value = 'SDR Qualified' then id else null end)) sdr_leads,
		round(sum(amount),1) opp_created_amount
	from (
		select
			l.region_c ,l.lead_source , l.pi_campaign_c , l.id , l.created_date lead_created , usr.id user_id, usr.name rep,
			l.converted_opportunity_id, o.amount ,
			l.lead_type_c , l.type_of_business_c , lh.created_date history_created , lh.field , lh.new_value , l.status ,
			rank() over (partition by l.id, new_value order by lh.created_date) rnk 
		from
		    claroty_salesforce.lead as l
		left join claroty_salesforce.lead_history lh on l.id = lh.lead_id 
		left join claroty_salesforce.user as usr on usr.id = l.owner_id
		left join claroty_salesforce.opportunity o on l.converted_opportunity_id = o.id 
		where 1 = 1
			and l.is_deleted = false
			and extract (year from l.created_date) in (2022)
	) a
	group by 1,2
)
select a.user_id, a.rep, coalesce(mail_count,0) + coalesce(call_count,0) as "Outreach",
    coalesce(sqo,0) as "SQOs", 
	coalesce (sdr_leads,0) as "SDRs",
    coalesce (opp_created_amount,0) as "Opp Created Amount"
from sl_kpis a 
left join mail_tp b on a.user_id = b.user_id_salesforce
left join call_tp c on a.user_id = c.user_id_salesforce
